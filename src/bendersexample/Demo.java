package bendersexample;

import static bendersexample.Solution.MSTOSEC;
import bendersexample.Solution.Verbosity;
import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * Demo executes the code demonstrating Benders decomposition in CPLEX (12.7
 * or later).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @version 1.02
 */
public final class Demo {
  private static final int DEFAULTSEED = 72612;  // default random seed
  private static final int NUMBEROFTRIALS = 1;   // default number of trials
  /** ROUNDUP is the cutoff for rounding values of binary variables up. */
  public static final double ROUNDUP = 0.5;
  /**
   * ModelType enumerates the possible types of model to run.
   *
   * STANDARD = single MIP model (no decomposition).
   * MANUAL = master plus one subproblem, hard-coded.
   * ANNOTATED = master plus one subproblem, created using annotations.
   * AUTOMATIC = master plus one or more subproblems, created from a standard
   * model by CPLEX.
   * WORKERS = annotated model giving CPLEX the option to decompose the
   * subproblem into multiple disjoint subproblems.
  */
  private enum ModelType { STANDARD, MANUAL, ANNOTATED, AUTOMATIC, WORKERS };

  /**
   * Constructor.
   */
  private Demo() { }

  /**
   * Main method.
   *
   * The program recognizes two optional command line arguments, both integer.
   * The first, if present, overrides the default number of trials to run (1).
   * The second, if present, overrides the default initial random seed.
   *
   * Edit the solution verbosity level and solver output levels in the
   * individual calls to runModel.
   *
   * Note: We request garbage collection after each model is tried, in an
   * attempt to ensure that run times for later models are not stretched
   * because the later models are squeezed for memory.
   *
   * @param args the command line arguments (not used)
   */
  public static void main(final String[] args) {
    int seed = DEFAULTSEED;        // initial seed for the problem generator
    int nTrials = NUMBEROFTRIALS;  // number of trials to run
    // Check for user inputs on the command line.
    if (args.length >= 1) {
      try {
        int nt = Integer.parseInt(args[0]);
        if (nt > 0) {
          nTrials = nt;
        }
      } catch (NumberFormatException ex) {
        System.out.println("First command line argument is not an integer.");
      }
    }
    if (args.length >= 2) {
      try {
        int s = Integer.parseInt(args[1]);
        seed = s;
      } catch (NumberFormatException ex) {
        System.out.println("Second command line argument is not an integer.");
      }
    }
    // Run the specified number of trials.
    for (int trial = 0; trial < nTrials; trial++) {
      System.out.println("\n\n+++++++++++++++++++");
      System.out.println("Trial " + trial + "\n");
      // Generate the problem instance.
      Problem problem = new Problem(seed);
      // Set up and solve a standard MIP model, to serve as a benchmark.
      runModel(problem, ModelType.STANDARD, "Standard Model",
               0, Verbosity.BASIC, false, seed);
      // Set up and solve a "traditional" Benders decomposition (separate master
      // and subproblem).
      runModel(problem, ModelType.MANUAL, "Manual Decomposition",
               0, Verbosity.BASIC, false, seed);
      // Use the "full Benders" feature to automatically implement Benders
      // decomposition on a standard MIP model.
      runModel(problem, ModelType.AUTOMATIC, "Automatic Decomposition",
               0, Verbosity.BASIC, false, seed);
      // Use an annotated model with a single subproblem.
      runModel(problem, ModelType.ANNOTATED, "Annotated Decomposition",
               0, Verbosity.BASIC, false, seed);
      // Finally, try the annotated model letting CPLEX look for further
      // decomposition of the subproblems.
      runModel(problem, ModelType.WORKERS, "Workers Decomposition",
               0, Verbosity.BASIC, false, seed);
      seed += 1;
      System.out.println("\n+++++++++++++++++++");
    }
  }

  /**
   * Run a model and print the results.
   *
   * @param problem the problem to solve
   * @param type the type of model to run
   * @param title the title to use in the output
   * @param outputLevel 0 to suppress solver output, 1 to show master problem
   * output only, 2 to show both master and subproblem output
   * @param verbosity the desired verbosity of the solution report
   * @param showCuts manual decomposition only: show generated cuts?
   * @param seed random seed to be used by CPLEX
   */
  private static void runModel(final Problem problem, final ModelType type,
                               final String title, final int outputLevel,
                               final Verbosity verbosity,
                               final boolean showCuts,
                               final int seed) {
    Model model;
    System.out.println("\n==========\nRunning " + title);
    long time = System.currentTimeMillis();
    try {
      switch (type) {
        case ANNOTATED:
          model = new AnnotatedBenders(problem);
          break;
        case MANUAL:
          model = new ManualBenders(problem, showCuts);
          break;
        case STANDARD:
          model = new StandardModel(problem);
          break;
        case AUTOMATIC:
          model = new StandardModel(problem);
          model.setBendersStrategy(IloCplex.BendersStrategy.Full);
          break;
        case WORKERS:
          model = new AnnotatedBenders(problem);
          model.setBendersStrategy(IloCplex.BendersStrategy.Workers);
          break;
        default:
          throw new IllegalArgumentException("Invalid model type specified.");
      }
    } catch (IloException ex) {
      System.out.println("Failed to build the model:\n" + ex.getMessage());
      return;
    }
    // Record the time to build the model
    time = System.currentTimeMillis() - time;
    System.out.println("Time to build model = " + MSTOSEC * time + " secs.");
    // Set the output level.
    model.setOutputLevel(outputLevel);
    Solution s;
    try {
      s = model.solve();
      System.out.println(s.report(verbosity));
    } catch (IloException ex) {
      System.out.println("Solution failed:\n" + ex.getMessage());
    }
    // Clean up.
    model.end();
    System.gc();
  }

}
