package bendersexample;

import ilog.cplex.IloCplex.CplexStatus;
import java.util.HashSet;
import java.util.Set;

/**
 * Solution is a container for solutions to the test problem. It provides a
 * mechanism for displaying them to the standard output stream.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Solution {
  /**
   * Verbosity controls the amount of output.
   */
  public enum Verbosity {
    /** BASIC shows solution time, cost and solver status only. */
    BASIC,
    /** WAREHOUSES adds a list of warehouses used to the basic output. */
    WAREHOUSES,
    /**FLOWS adds a list of nonzero flows to the WAREHOUSES output. */
    FLOWS
  };
  /**
   * FUZZ limits how small a flow can be and still be treated as nonzero.
  */
  public static final double FUZZ = 1e-6;
  /** MSTOSEC is the milliseconds to seconds conversion factor. */
  public static final double MSTOSEC = 0.001;

  private double cost;                   // total cost
  private final double[][] flows;        // flow volumes
  private HashSet<Integer> warehouses;   // warehouses used
  private CplexStatus status;            // status returned by CPLEX
  private double time;                   // solution time (seconds)

  /**
   * Constructor.
   * @param nw the number of warehouses in the problem
   * @param nc the number of customers in the problem
   */
  public Solution(final int nw, final int nc) {
    flows = new double[nw][nc];
  }

  /**
   * Set the total cost.
   * @param c the total cost
   */
  public void setCost(final double c) {
    cost = c;
  }

  /**
   * Set an arc flow.
   * @param w the warehouse
   * @param c the customer
   * @param f the flow from the warehouse to the customer
   */
  public void setFlow(final int w, final int c, final double f) {
    flows[w][c] = f;
  }

  /**
   * Set the choice of warehouses to open.
   * @param w the set of warehouses to open
   */
  public void setWarehouses(final Set<Integer> w) {
    this.warehouses = new HashSet<>(w);
  }

  /**
   * Set the solver status.
   * @param s the solver status
   */
  public void setStatus(final CplexStatus s) {
    this.status = s;
  }

  /**
   * Set the solution time.
   * @param t the solution time (in ms.)
   */
  public void setTime(final long t) {
    this.time = MSTOSEC * t;
  }

  /**
   * Generate a string reporting whatever portion of the solution the user
   * wishes to see.
   * @param v the desired verbosity of the report
   * @return the report
   */
  public String report(final Verbosity v) {
    StringBuilder sb = new StringBuilder();
    // Start with the basic output.
    sb.append("Final solver status = ")
      .append(status)
      .append("\n# of warehouses used = ")
      .append(warehouses.size())
      .append("\nTotal cost = ")
      .append(cost)
      .append(String.format("\nSolution time (seconds) = %.3f\n", time));
    if (v == Verbosity.BASIC) {
      return sb.toString();
    }
    // The verbosity is at least WAREHOUSES: add a list of warehouses.
    sb.append("\nWarehouses used:\n");
    warehouses.stream().sorted().forEach((w) -> {
      sb.append("\t").append(w);
    });
    sb.append("\n");
    if (v == Verbosity.WAREHOUSES) {
      return sb.toString();
    }
    // The verbosity is FLOWS: add the nonzero flows.
    sb.append("\nNonzer flows:");
    for (int i = 0; i < flows.length; i++) {
      for (int j = 0; j < flows[0].length; j++) {
        if (flows[i][j] > FUZZ) {
          sb.append(String.format("\t%d -> %d : %f\n", i, j, flows[i][j]));
        }
      }
    }
    return sb.toString();
  }
}
