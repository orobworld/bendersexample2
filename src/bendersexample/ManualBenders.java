package bendersexample;

import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.util.HashMap;
import java.util.HashSet;

/**
 * ManualBenders builds and solves a model using Benders decomposition with
 * separate master and subproblems.
 *
 * The master problem (MIP) selects the warehouses to use.
 *
 * The subproblem (LP) determines flows from warehouses to customers.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class ManualBenders implements Model {
  private final int nWarehouses;       // number of warehouses
  private final int nCustomers;        // number of customers
  private final boolean showCuts;      // print cut details?
  private final IloCplex master;       // the master model
  private final IloCplex sub;          // the subproblem
  private final IloNumVar[] use;       // binary variables selecting warehouses
  private final IloNumVar[][] ship;    // flow variables
  private final IloNumVar flowCost;    // surrogate variable for flow costs
  private final double[] capacity;     // warehouse capacities
  private final double[] demand;       // customer demands
  private final double[][] flowValues; // subproblem flow values
  private double incumbentValue;       // value of the current incumbent
  /*
   * To compute both optimality and feasibility cuts, we will need to multiply
   * the right-hand sides of the subproblem constraints (including both constant
   * terms and terms involving master problem variables) by the corresponding
   * subproblem dual values (obtained either directly, if the subproblem is
   * optimized, or via a Farkas certificate if the subproblem is infeasible).
   * To facilitate the computations, we will construct an instance of IloNumExpr
   * for each right-hand side, incorporating both scalars and master variables.
   *
   * Since CPLEX returns the Farkas certificate in no particular order (in
   * particular, NOT in the order the subproblem constraints are created),
   * we need to be able to access the IloNumExpr instances in the same arbitrary
   * order used by the Farkas certificate. To do that, we will store them in
   * a map keyed by the subproblem constraints themselves (which we will store
   * in arrays).
   */
  private final IloRange[] cSupply;                      // supply constraints
  private final IloRange[] cDemand;                      // demand constraints
  private final HashMap<IloConstraint, IloNumExpr> rhs;  // right hand sides

  /**
   * Constructor.
   * @param problem the problem to solve
   * @param sc if true, show cut details
   * @throws IloException if either master or subproblem cannot be constructed
   */
  public ManualBenders(final Problem problem, final boolean sc)
         throws IloException {
    showCuts = sc;
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    rhs = new HashMap<>();
    flowValues = new double[nWarehouses][nCustomers];
    // Record capacities and demands and get fixed costs.
    capacity = new double[nWarehouses];
    demand = new double[nCustomers];
    double[] fixed = new double[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      capacity[w] = problem.getCapacity(w);
      fixed[w] = problem.getFixedCost(w);
    }
    for (int c = 0; c < nCustomers; c++) {
      demand[c] = problem.getDemand(c);
    }
    // Initialize the master and subproblems.
    master = new IloCplex();
    sub = new IloCplex();
    // Set up the master problem (which initially has no constraints).
    String[] names = new String[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      names[w] = "Use_" + w;
    }
    use = master.boolVarArray(nWarehouses, names);
    flowCost = master.numVar(0.0, Double.MAX_VALUE, "estFlowCost");
    master.addMinimize(master.sum(flowCost, master.scalProd(fixed, use)),
                       "TotalCost");
    // Attach a Benders callback to the master.
    master.use(new BendersCallback());
    // Set up the subproblem objective.
    ship = new IloNumVar[nWarehouses][nCustomers];
    IloLinearNumExpr expr = sub.linearNumExpr();
    for (int w = 0; w < nWarehouses; w++) {
      for (int c = 0; c < nCustomers; c++) {
        ship[w][c] = sub.numVar(0.0, Double.MAX_VALUE, "Flow_" + w + "_" + c);
        expr.addTerm(problem.getFlowCost(w, c), ship[w][c]);
      }
    }
    // Minimize total flow cost.
    sub.addMinimize(expr, "FlowCost");
    // Constrain demand to be satisfied -- record the constraints for use later.
    cDemand = new IloRange[nCustomers];
    for (int c = 0; c < nCustomers; c++) {
      expr.clear();
      for (int w = 0; w < nWarehouses; w++) {
        expr.addTerm(1.0, ship[w][c]);
      }
      cDemand[c] = sub.addGe(expr, demand[c], "Demand_" + c);
      rhs.put(cDemand[c], master.linearNumExpr(demand[c]));
    }
    // Add supply limits (initially all zero, which makes the subproblem
    // infeasible):
    // -- record the constraints for use later;
    // -- also map each constraint to the corresponding binary variable in the
    //    master (for decoding Farkas certificates).
    cSupply = new IloRange[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      cSupply[w] = sub.addLe(sub.sum(ship[w]), 0.0, "Supply_" + w);
      rhs.put(cSupply[w], master.prod(capacity[w], use[w]));
    }
    // Disable presolving of the subproblem (if the presolver recognizes that
    // the subproblem is infeasible, we do not get a dual ray).
    sub.setParam(IloCplex.BooleanParam.PreInd, false);
    // Force use of the dual simplex algorithm to get a Farkas certificate.
    // (The dualFarkas method requires that the dual simplex algorithm be used.)
    sub.setParam(IloCplex.IntParam.RootAlg, 2);
    // Suppress master and subproblem output by default.
    master.setOut(null);
    sub.setOut(null);
  }

  /**
   * Solve the model.
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  @Override
  public Solution solve() throws IloException {
    incumbentValue = Double.MAX_VALUE;
    Solution s = new Solution(nWarehouses, nCustomers);
    long start = System.currentTimeMillis();
    if (master.solve()) {
      // Record the solution.
      s.setTime(System.currentTimeMillis() - start);
      s.setCost(master.getObjValue());
      s.setStatus(master.getCplexStatus());
      HashSet<Integer> warehouses = new HashSet<>();
      double[] u = master.getValues(use);
      for (int w = 0; w < nWarehouses; w++) {
        for (int c = 0; c < nCustomers; c++) {
          s.setFlow(w, c, flowValues[w][c]);
        }
        if (u[w] > Demo.ROUNDUP) {
          warehouses.add(w);
        }
      }
      s.setWarehouses(warehouses);
    } else {
      // Record only the solution time and status.
      s.setTime(System.currentTimeMillis() - start);
      s.setStatus(master.getCplexStatus());
    }
    return s;
  }

  /**
   * Set the Benders strategy.
   *
   * This operation is unsupported for the manual method.
   *
   * @param strategy the new strategy
   * @throws IloException if setting the strategy fails.
   */
  @Override
  public void setBendersStrategy(final int strategy) throws IloException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  /**
   * Set the output level for the solver.
   *
   * @param level the desired output level
   * (0 = none [default], 1 = master only, 2 = master and subproblems)
   */
  @Override
  public void setOutputLevel(final int level) {
    switch (level) {
      case 0:  // suppress all output
        master.setOut(null);
        sub.setOut(null);
        break;
      case 1:  // master output only
        master.setOut(System.out);
        sub.setOut(null);
        break;
      case 2:  // master and subproblem output (which is just master in
               // this case)
        master.setOut(System.out);
        sub.setOut(System.out);
        break;
      default: // illegal argument
        throw new IllegalArgumentException("Invalid output level specified.");
    }
  }

  /**
   * Set the random number seed for the solver.
   * @param seed the seed to use
   * @throws IloException if the seed cannot be set
   */
  @Override
  public void setSeed(final int seed) throws IloException {
    master.setParam(IloCplex.IntParam.RandomSeed, seed);
    sub.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Clean up any objects attached to the model.
   *
   */
  @Override
  public void end() {
    master.end();
    sub.end();
  }

  /**
   * BendersCallback implements a lazy constraint callback that uses the
   * warehouse selection decisions in a proposed new incumbent solution to the
   * master problem to solve the subproblem.
   *
   * If the subproblem is optimized and the subproblem objective cost matches
   * the incumbent's estimated flow cost (to within rounding tolerance), the
   * callback exits without adding a constraint (resulting in the incumbent
   * being accepted in the master problem).
   *
   * If the subproblem is optimized but the incumbent's estimated flow cost
   * underestimates the true flow cost, the callback adds an optimality cut
   * (eliminating the proposed incumbent).
   *
   * If the subproblem proves to be infeasible, the callback adds a feasibility
   * cut (eliminating the proposed incumbent).
   *
   * Those should be the only possible outcomes. If something else happens
   * (subproblem unbounded or unsolved), the callback writes a message to
   * stderr and punts.
   */
  private class BendersCallback extends IloCplex.LazyConstraintCallback {

    @Override
    protected void main() throws IloException {
      // Get the master flow cost estimate.
      double zMaster = getValue(flowCost);
      // Which warehouses does the proposed master solution use?
      double[] x = getValues(use);
      // Set the supply constraint right-hand sides in the subproblem.
      for (int w = 0; w < nWarehouses; w++) {
        cSupply[w].setUB((x[w] >= Demo.ROUNDUP) ? capacity[w] : 0);
      }
      // Solve the subproblem.
      sub.solve();
      IloCplex.Status status = sub.getStatus();
      IloNumExpr expr = master.numExpr();
      if (status == IloCplex.Status.Infeasible) {
        // Subproblem is infeasible -- add a feasibility cut.
        // First step: get a Farkas certificate, corresponding to a dual ray
        // along which the dual is unbounded.
        IloConstraint[] constraints =
          new IloConstraint[nWarehouses + nCustomers];
        double[] coefficients = new double[nWarehouses + nCustomers];
        sub.dualFarkas(constraints, coefficients);
        // Process all elements of the Farkas certificate.
        for (int i = 0; i < constraints.length; i++) {
          IloConstraint c = constraints[i];
          expr = master.sum(expr, master.prod(coefficients[i], rhs.get(c)));
        }
        // Add a feasibility cut.
        IloConstraint r = add(master.le(expr, 0));
        System.out.println("\n>>> Adding feasibility cut");
        if (showCuts) {
          System.out.println(r);
        }
        System.out.println();
      } else if (status == IloCplex.Status.Optimal) {
        if (zMaster < sub.getObjValue() - Solution.FUZZ) {
          // The master problem surrogate variable underestimates the actual
          // flow cost -- add an optimality cut.
          double[] lambda = sub.getDuals(cDemand);
          double[] mu = sub.getDuals(cSupply);
          // Compute the scalar product of the RHS of the demand constraints
          // with the duals for those constraints.
          for (int c = 0; c < nCustomers; c++) {
            expr = master.sum(expr, master.prod(lambda[c],
                              rhs.get(cDemand[c])));
          }
          // Repeat for the supply constraints.
          for (int w = 0; w < nWarehouses; w++) {
            expr = master.sum(expr, master.prod(mu[w], rhs.get(cSupply[w])));
          }
          // Add the optimality cut.
          IloConstraint r = add((IloRange) master.ge(flowCost, expr));
          System.out.println("\n>>> Adding optimality cut");
          if (showCuts) {
            System.out.println(r);
          }
          System.out.println();
        } else {
          System.out.println("\n>>> Accepting new incumbent with value "
                             + getObjValue() + "\n");
          // The master and subproblem flow costs match; IF this is a new
          // incumbent, record the subproblem flows in case this proves to be
          // the winner (saving us from having to solve the LP one more time
          // once the master terminates).
          double value = getObjValue();
          if (value < incumbentValue) {
            incumbentValue = value;
            for (int w = 0; w < nWarehouses; w++) {
              flowValues[w] = sub.getValues(ship[w]);
            }
          }
        }
      } else {
        // An unexpected status occurred -- report it but do nothing.
        System.err.println("\n!!! Unexpected subproblem solution status: "
                           + status + "\n");
      }
    }
  }
}
