package bendersexample;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.HashSet;

/**
 * StandardModel provides a standard MIP model for the problem, to be used as
 * a benchmark.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class StandardModel implements Model {
  private final IloCplex cplex;   // the MIP model
  private final IloNumVar[] use;  // use[i] = 1 if warehouse i is used, 0 if not
  private final IloNumVar[][] ship;
                                  // ship[i][j] is the flow from warehouse i
                                  // to customer j
  private final int nWarehouses;  // number of warehouses
  private final int nCustomers;   // number of customers

  /**
   * Constructor.
   * @param problem the problem to be solved
   * @throws IloException if the model cannot be built
   */
  public StandardModel(final Problem problem) throws IloException {
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    cplex = new IloCplex();
    use = new IloNumVar[nWarehouses];
    ship = new IloNumVar[nWarehouses][nCustomers];
    IloLinearNumExpr expr = cplex.linearNumExpr();
    // Declare the variables and simultaneously assemble the objective function.
    for (int w = 0; w < nWarehouses; w++) {
      use[w] = cplex.boolVar("Use" + w);
      expr.addTerm(problem.getFixedCost(w), use[w]);
      for (int c = 0; c < nCustomers; c++) {
        ship[w][c] = cplex.numVar(0.0,
                                  Math.min(problem.getCapacity(w),
                                           problem.getDemand(c)),
                                  "Ship_" + w + "_" + c);
        expr.addTerm(problem.getFlowCost(w, c), ship[w][c]);
      }
    }
    // Minimize total cost.
    cplex.addMinimize(expr, "TotalCost");
    // Add demand constraints.
    for (int c = 0; c < nCustomers; c++) {
      expr.clear();
      for (int w = 0; w < nWarehouses; w++) {
        expr.addTerm(1.0, ship[w][c]);
      }
      cplex.addGe(expr, problem.getDemand(c), "Demand_" + c);
    }
    // Add supply constraints.
    for (int w = 0; w < nWarehouses; w++) {
      cplex.addLe(cplex.sum(ship[w]),
                  cplex.prod(problem.getCapacity(w), use[w]),
                  "Supply_" + w);
    }
    // Suppress output by default.
    cplex.setOut(null);
  }

  /**
   * Solve the model.
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  @Override
  public Solution solve() throws IloException {
    Solution s = new Solution(nWarehouses, nCustomers);
    long start = System.currentTimeMillis();
    if (cplex.solve()) {
      // Fill in the solution.
      s.setTime(System.currentTimeMillis() - start);
      s.setCost(cplex.getObjValue());
      s.setStatus(cplex.getCplexStatus());
      HashSet<Integer> warehouses = new HashSet<>();
      double[] u = cplex.getValues(use);
      for (int w = 0; w < use.length; w++) {
        double[] flows = cplex.getValues(ship[w]);
        for (int c = 0; c < nCustomers; c++) {
          s.setFlow(w, c, flows[c]);
        }
        if (u[w] > Demo.ROUNDUP) {
          warehouses.add(w);
        }
      }
      s.setWarehouses(warehouses);
    } else {
      // Fill in just the time and status.
      s.setTime(System.currentTimeMillis() - start);
      s.setStatus(cplex.getCplexStatus());
    }
    return s;
  }

  /**
   * Set the Benders strategy for the model.
   * @param strategy the new Benders strategy
   * @throws IloException if CPLEX balks at setting the strategy
   */
  @Override
  public void setBendersStrategy(final int strategy) throws IloException {
    cplex.setParam(IloCplex.Param.Benders.Strategy, strategy);
  }

  /**
   * Set the output level for the solver.
   *
   * @param level the desired output level
   * (0 = none [default], 1 = master only, 2 = master and subproblems)
   */
  @Override
  public void setOutputLevel(final int level) {
    switch (level) {
      case 0:  // suppress all output
        cplex.setOut(null);
        break;
      case 1:  // master output only
      case 2:  // master and subproblem output (which is just master in
               // this case)
        cplex.setOut(System.out);
        break;
      default: // illegal argument
        throw new IllegalArgumentException("Invalid output level specified.");
    }
  }

  /**
   * Set the random number seed for the solver.
   * @param seed the seed to use
   * @throws IloException if the seed cannot be set
   */
  @Override
  public void setSeed(final int seed) throws IloException {
    cplex.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Clean up any objects attached to the model.
   *
   */
  @Override
  public void end() {
    cplex.end();
  }
}
