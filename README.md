# BendersExample2 #

### What is this repository for? ###

The code here provides a small example of how to implement Benders decomposition using the Java API to CPLEX. The application is a fixed charge transportation problem. The code requires CPLEX 12.7 or later, and uses the support for Benders decomposition that is built into CPLEX.

The code actually contains five different approaches to solving the problem, as follows:

* STANDARD: Use a single MIP model (no decomposition).
* MANUAL: Code two separate models (master and subproblem), as one would do prior to CPLEX 12.7.
* ANNOTATED: Create a single model, using the annotation method added to CPLEX 12.7 to tell CPLEX how to split it into a master problem and one subproblem.
* WORKERS: Use the ANNOTATED method, but give CPLEX the option to then split the subproblem into two or more subproblems if CPLEX deems that workable.
* AUTOMATIC: Create a single model (same as the STANDARD method) and let CPLEX decide how to split it into a master problem and one or more subproblems.

### Version ###
The current version of the example code (in the "master" branch) is 1.02.
This differs from version 1.0 in some minor edits of comments, removal of
one unused bit of code, and a significant bug fix (ensuring that the subproblem
solution stored in the manual Benders callback matched the correct incumbent
solution).

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

### Setup ###

* If you don't already have it, install CPLEX Studio. (CPLEX is the only dependency.) You must use version 12.7 or later.
* Download the code from the repository (seven Java classes) and park it somewhere.
* To run the code from a command line, open a terminal/DOS window, change to the directory where you parked the code, and execute the following:

<pre style="margin: 1em 10em;">
java -Djava.library.path=<path to CPLEX binary> -jar BendersExample2.jar
</pre>

You can omit the library path argument if the CPLEX binary is on the system library path (given by the LD_LIBRARY_PATH in Linux).

### Problems? ###

I've set up an issue tracker in case anyone runs into problems.
